<?php

namespace Metasite\Component\Timestampable;

use Datetime;
use DateTimeZone;

trait TimestampableTrait
{
    /**
     * @var string
     */
    protected $createdAt;

    /**
     * @var string
     */
    protected $updatedAt;

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return $this
     */
    public function setCreatedAt()
    {
        $createdAt = new DateTime('now', new DateTimeZone('UTC'));
        $this->createdAt = $createdAt->format("Y-m-d H:i:s");

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return $this
     */
    public function setUpdatedAt()
    {
        $updatedAt = new DateTime('now', new DateTimeZone('UTC'));
        $this->updatedAt = $updatedAt->format("Y-m-d H:i:s");

        return $this;
    }
}
