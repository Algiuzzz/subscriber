<?php

namespace Metasite\Bundle\AdminBundle\Controller;

use Metasite\Bundle\SubscribeBundle\Form\Type\SubscribeType;
use Metasite\Bundle\SubscribeBundle\Services\SubscriberService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SubscriberController
 * @package Metasite\Bundle\AdminBundle\Controller
 */
class SubscriberController extends Controller
{
    /**
     * @Route("/", name="admin_homepage")
     */
    public function indexAction(Request $request)
    {
        $sort = $request->get('sort', null);
        $sortType = $request->get('sorttype', 'ASC');

        $data = $this->getSubscriberService()->getAll($sort, $sortType);

        return $this->render(
            '@MetasiteAdmin/subscriber/index.html.twig',
            [
                'data' => $data,
            ]
        );
    }

    /**
     * @Route("/subscriber/delete/{uuid}", name="subscriber_delete")
     *
     * @param string $uuid
     * @return RedirectResponse
     */
    public function deleteAction(string $uuid)
    {
        $this->getSubscriberService()->delete($uuid);

        $this->addFlash(
            'success',
            $this->container->get('translator')->trans('metasite.admin.subscriber.success_delete')
        );

        return $this->redirectToRoute('admin_homepage');
    }

    /**
     * @Route("/subscriber/update/{uuid}", name="subscriber_update")
     *
     * @param Request $request
     * @param string $uuid
     * @return Response|RedirectResponse
     */
    public function updateAction(Request $request, string $uuid)
    {
        $subscriber = $this->getSubscriberService()->getOne($uuid);

        $form = $this->createForm(
            SubscribeType::class,
            $subscriber
        );

        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted() && $form->isValid()) {

                $this->getSubscriberService()->update($form->getData());

                $this->addFlash(
                    'success',
                    $this->container->get('translator')->trans('metasite.admin.subscriber.success_update')
                );

                return $this->redirectToRoute('admin_homepage');
            }
        }


        return $this->render(
            '@MetasiteAdmin/subscriber/update.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @return SubscriberService
     */
    private function getSubscriberService(): SubscriberService
    {
        return $this->container->get('metasite_subscriber.subscriber_service');
    }
}
