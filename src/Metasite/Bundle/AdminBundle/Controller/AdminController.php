<?php

namespace Metasite\Bundle\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AdminController
 * @package Metasite\Bundle\AdminBundle\Controller
 */
class AdminController extends Controller
{
    /**
     * @Route("/login", name="login")
     *
     * @return RedirectResponse|Response
     */
    public function loginAction()
    {
        $user = $this->getUser();
        if ($user instanceof UserInterface) {
            return $this->redirectToRoute('admin_homepage');
        }

        /** @var AuthenticationException $exception */
        $exception = $this->get('security.authentication_utils')
            ->getLastAuthenticationError();

        return $this->render(
            '@MetasiteAdmin/admin/login.html.twig',
            [
                'error' => $exception ? $exception->getMessage() : null,
            ]
        );
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request)
    {
        // Nothing needed.
    }
}
