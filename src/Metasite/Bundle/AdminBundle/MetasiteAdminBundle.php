<?php

namespace Metasite\Bundle\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class MetasiteAdminBundle
 * @package Metasite\Bundle\AdminBundle
 */
class MetasiteAdminBundle extends Bundle
{
}
