<?php

namespace Metasite\Bundle\SubscribeBundle\Model;

use Metasite\Component\Timestampable\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Subscriber
 * @package Metasite\Bundle\SubscribeBundle\Model
 */
class Subscriber
{
    use TimestampableTrait;

    /**
     * @var string
     */
    private $uuid;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     */
    private $category;

    /**
     * Task constructor.
     */
    public function __construct()
    {
        $this->uuid = uniqid();
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     * @return Subscriber
     */
    public function setUuid(string $uuid): Subscriber
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Subscriber
     */
    public function setName($name): Subscriber
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Subscriber
     */
    public function setEmail(string $email): Subscriber
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param int $category
     * @return Subscriber
     */
    public function setCategory(int $category): Subscriber
    {
        $this->category = $category;

        return $this;
    }
}
