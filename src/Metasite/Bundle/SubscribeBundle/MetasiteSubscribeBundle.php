<?php

namespace Metasite\Bundle\SubscribeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class MetasiteSubscribeBundle
 * @package Metasite\Bundle\SubscribeBundle
 */
class MetasiteSubscribeBundle extends Bundle
{
}
