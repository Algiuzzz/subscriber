<?php

namespace Metasite\Bundle\SubscribeBundle\Services;

use Metasite\Bundle\SubscribeBundle\Model\Subscriber;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class SubscriberService
 * @package Metasite\Bundle\SubscribeBundle\Services
 */
class SubscriberService
{
    const DATA_FORMAT = 'json';
    const EXCEPTION_LOG = 'SubscriberService ';

    /** @var string */
    private $dataFilePath;

    /** @var Filesystem */
    private $fs;

    /** @var Serializer */
    private $serializer;

    /** @var LoggerInterface */
    private $logger;

    /**
     * SubscriberService constructor.
     * @param string $dataFilePath
     * @param Filesystem $fs
     * @param LoggerInterface $logger
     */
    public function __construct(string $dataFilePath, Filesystem $fs, LoggerInterface $logger)
    {
        $this->dataFilePath = $dataFilePath;
        $this->fs = $fs;
        $this->logger = $logger;
    }

    /**
     * Insert into file
     * @param Subscriber $subscriber
     * @return bool
     */
    public function persist(Subscriber $subscriber)
    {
        try {
            $subscriber->setCreatedAt();
            $subscriber->setUpdatedAt();

            $data = $this->getAll();
            $data[] = $subscriber;
            $jsonContent = $this->serializer->serialize($data, self::DATA_FORMAT);

            $this->fs->dumpFile($this->dataFilePath, $jsonContent);
        }
        catch(Exception $e) {
            $this->logger->critical(self::EXCEPTION_LOG . $e->getMessage(), ['function' => __FUNCTION__]);
        }

        return true;
    }

    /**
     * Get one
     * @param string $uuid
     * @return Subscriber|null
     */
    public function getOne(string $uuid)
    {
        try {
            $data = $this->getAll();
            $key = $this->getDataKey($uuid, $data);

            return $data[$key];
        }
        catch(Exception $e) {
            $this->logger->critical(self::EXCEPTION_LOG . $e->getMessage(), ['function' => __FUNCTION__]);
        }

        return null;
    }

    /**
     * Get all data
     * @param string $sort
     * @param string $sortType
     * @return array
     */
    public function getAll($sort = null, $sortType = 'ASC'): array
    {
        $result = [];

        try {
            $data = file_get_contents($this->dataFilePath);
            $result = $this->serializer->deserialize($data, Subscriber::class . '[]', self::DATA_FORMAT);
            $result = $this->sort($sort, $sortType, $result);
        }
        catch(Exception $e) {
            $this->logger->critical(self::EXCEPTION_LOG . $e->getMessage(), ['function' => __FUNCTION__]);
        }

        return $result;
    }

    /**
     * Delete element from file
     * @param string $uuid
     */
    public function delete($uuid)
    {
        try {
            $data = $this->getAll();
            $key = $this->getDataKey($uuid, $data);

            unset($data[$key]);

            $data = array_values($data);

            $jsonContent = $this->serializer->serialize($data, self::DATA_FORMAT);
            $this->fs->dumpFile($this->dataFilePath, $jsonContent);
        }
        catch(Exception $e) {
            $this->logger->critical(self::EXCEPTION_LOG . $e->getMessage(), ['function' => __FUNCTION__]);
        }
    }

    /**
     * Update element from file
     * @param Subscriber $subscriber
     */
    public function update(Subscriber $subscriber)
    {
        try {
            $data = $this->getAll();
            $key = $this->getDataKey($subscriber->getUuid(), $data);

            $data[$key]->setName($subscriber->getName());
            $data[$key]->setEmail($subscriber->getEmail());
            $data[$key]->setCategory($subscriber->getCategory());
            $data[$key]->setUpdatedAt();

            $jsonContent = $this->serializer->serialize($data, self::DATA_FORMAT);

            $this->fs->dumpFile($this->dataFilePath, $jsonContent);
        }
        catch(Exception $e) {
            $this->logger->critical(self::EXCEPTION_LOG . $e->getMessage(), ['function' => __FUNCTION__]);
        }
    }

    /**
     * Serializer initialization
     */
    public function initSerializer()
    {

        $normalizers = [
            new PropertyNormalizer(),
            new ArrayDenormalizer(),
        ];

        $encoders = [
            new JsonEncoder()
        ];

        $this->serializer = new Serializer($normalizers, $encoders);
    }

    /**
     * Get key by uuid
     * @param string $uuid
     * @param array $data
     * @return int
     */
    private function getDataKey(string $uuid, array $data)
    {
        $normalizeData = $this->serializer->normalize($data, self::DATA_FORMAT);
        if (($key = array_search($uuid, array_column($normalizeData, 'uuid'))) !== false) {
            return $key;
        }

        throw new NotFoundHttpException();
    }

    /**
     * Sorting
     * @param string $sort
     * @param string $sortType
     * @param array $data
     * @return array
     */
    private function sort(string $sort = null, string $sortType = 'ASC', array $data)
    {
        if (null === $sort) {
            return $data;
        }

        $data = $this->serializer->normalize($data, self::DATA_FORMAT);

        usort($data, function($a, $b) use ($sort) {
            return strcasecmp($a[$sort], $b[$sort]);
        });

        if ($sortType == 'DESC') {
            $data = array_reverse($data);
        }

        $data = $this->serializer->serialize($data, self::DATA_FORMAT);
        $data = $this->serializer->deserialize($data, Subscriber::class . '[]', self::DATA_FORMAT);

        return $data;
    }
}
