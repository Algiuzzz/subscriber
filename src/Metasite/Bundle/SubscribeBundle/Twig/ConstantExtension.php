<?php

namespace Metasite\Bundle\SubscribeBundle\Twig;

use Metasite\Bundle\SubscribeBundle\Constants\SubscriberCategory;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ConstantExtension
 * @package Metasite\Bundle\SubscribeBundle\Twig
 */
class ConstantExtension extends \Twig_Extension
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * ConstantExtension constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('subscriber_category_constant', [$this, 'constantFilter']),
        ];
    }

    /**
     * Getting subscriber category
     * @param string $value
     * @return string
     */
    public function constantFilter($value)
    {
        $formChoices = SubscriberCategory::getFormChoices();
        $formChoices = array_flip($formChoices);

        if (isset($formChoices[$value])) {
            return $this->translator->trans($formChoices[$value]);
        }

        return $value;
    }
}
