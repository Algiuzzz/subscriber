<?php

namespace Metasite\Bundle\SubscribeBundle\Controller;

use Metasite\Bundle\SubscribeBundle\Form\Type\SubscribeType;
use Metasite\Bundle\SubscribeBundle\Model\Subscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SubscribeController
 * @package Metasite\Bundle\SubscribeBundle\Controller
 */
class SubscribeController extends Controller
{
    /**
     * @Route("/", name="subscriber")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $subscriber = new Subscriber();

        $form = $this->createForm(
            SubscribeType::class,
            $subscriber
        );

        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted() && $form->isValid()) {

                $this->container->get('metasite_subscriber.subscriber_service')->persist($form->getData());

                $this->addFlash(
                    'success',
                    $this->container->get('translator')->trans('metasite.subscriber.success')
                );

                return $this->redirectToRoute('subscriber');
            }
        }


        return $this->render(
            '@MetasiteSubscribe/index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
