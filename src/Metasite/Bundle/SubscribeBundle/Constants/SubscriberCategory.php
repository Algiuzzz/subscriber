<?php

namespace Metasite\Bundle\SubscribeBundle\Constants;

/**
 * Class SubscriberCategory
 * @package Metasite\Bundle\SubscribeBundle\Constants
 */
final class SubscriberCategory
{
    const CATEGORY_FASHION = 1;
    const CATEGORY_TECHNOLOGY = 2;
    const CATEGORY_POLITICS = 3;

    /**
     * @return array
     */
    public static function getFormChoices(): array
    {
        return [
            'metasite.subscriber.category.fashion' => self::CATEGORY_FASHION ,
            'metasite.subscriber.category.technology' => self::CATEGORY_TECHNOLOGY,
            'metasite.subscriber.category.politics'=> self::CATEGORY_POLITICS,
        ];
    }
}
