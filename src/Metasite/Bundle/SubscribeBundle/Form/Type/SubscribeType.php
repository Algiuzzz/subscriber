<?php

namespace Metasite\Bundle\SubscribeBundle\Form\Type;

use Metasite\Bundle\SubscribeBundle\Constants\SubscriberCategory;
use Metasite\Bundle\SubscribeBundle\Model\Subscriber;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SubscribeType
 * @package Metasite\Bundle\SubscribeBundle\Form\Type
 */
class SubscribeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'metasite.subscriber.form.name',
                    'required' => true,
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'metasite.subscriber.form.email',
                    'required' => true,
                ]
            )
            ->add(
                'category',
                ChoiceType::class,
                [
                    'label' => 'metasite.subscriber.form.category',
                    'required' => true,
                    'choices' => SubscriberCategory::getFormChoices(),
                    'multiple' => false,
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'metasite.subscriber.form.button_subscribe',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Subscriber::class,
            ]
        );
    }
}
