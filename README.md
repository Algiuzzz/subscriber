Subscriber v 1.0.0
============

This is filesystem based, simple subscriber. This subscriber is not adapted for huge data.

You will find simple data admin in http://{your_domain}/admin

Account for testing: admin:admin

Demo data: app/data/subscribers.json

System Requirements
============

* PHP 7.*
* PHP CLI, the same version as for the web server
* Composer

Run project
============
 * Checkout project from repository
 * Run `composer install` to get vendors
 * Check dir and file permissions

Recommend TODO implementations 
============
* Implement duplicate email checking
* Implement list pagination
* Implement tests
